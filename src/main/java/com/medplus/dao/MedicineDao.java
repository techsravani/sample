package com.medplus.dao;

import java.util.List;

import com.medplus.bo.MedicinesBo;

public interface MedicineDao {
	List<MedicinesBo> getMedicines();
}
