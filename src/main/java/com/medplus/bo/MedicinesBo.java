package com.medplus.bo;

import java.io.Serializable;

public class MedicinesBo implements Serializable{

	private static final long serialVersionUID = 8609877861774052842L;
	
	protected int medicineNo;
	protected String medcineName;
	protected String manufacturer;
	protected float price;
	protected int quantity;
	protected int expiryMonth;
	protected int expiryYear;
	
	public int getMedicineNo() {
		return medicineNo;
	}
	public void setMedicineNo(int medicineNo) {
		this.medicineNo = medicineNo;
	}
	public String getMedcineName() {
		return medcineName;
	}
	public void setMedcineName(String medcineName) {
		this.medcineName = medcineName;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getExpiryMonth() {
		return expiryMonth;
	}
	public void setExpiryMonth(int expiryMonth) {
		this.expiryMonth = expiryMonth;
	}
	public int getExpiryYear() {
		return expiryYear;
	}
	public void setExpiryYear(int expiryYear) {
		this.expiryYear = expiryYear;
	}
}
