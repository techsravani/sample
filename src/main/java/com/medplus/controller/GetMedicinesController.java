package com.medplus.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.medplus.dto.MedicinesDto;
import com.medplus.service.MedicineService;

public class GetMedicinesController extends AbstractController{
	private MedicineService medicineService;

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		List<MedicinesDto> medicines=medicineService.getMedicines();
		ModelAndView mv=new ModelAndView();
		mv.addObject("medicines",medicines);
		mv.setViewName("medicines");
		return mv;
	}

	public void setMedicineService(MedicineService medicineService) {
		this.medicineService = medicineService;
	}
}
