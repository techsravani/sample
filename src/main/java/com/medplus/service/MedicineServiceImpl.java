package com.medplus.service;

import java.util.ArrayList;
import java.util.List;

import com.medplus.bo.MedicinesBo;
import com.medplus.dao.MedicineDao;
import com.medplus.dto.MedicinesDto;

public class MedicineServiceImpl implements MedicineService{
	private MedicineDao medicineDao;

	@Override
	public List<MedicinesDto> getMedicines() {
		MedicinesDto medicineDto=null;
		List<MedicinesBo> bos=null;
		List<MedicinesDto> dto=null;
		
		dto=new ArrayList<>();
		bos=medicineDao.getMedicines();
		
		for(MedicinesBo bo:bos) {
			medicineDto=new MedicinesDto();
			medicineDto.setMedicineName(bo.getMedcineName());
			medicineDto.setManufacturer(bo.getManufacturer());
			medicineDto.setPrice(bo.getPrice());
			medicineDto.setQuantity(bo.getQuantity());
			dto.add(medicineDto);
		}
		return dto;
	}

	public void setMedicineDao(MedicineDao medicineDao) {
		this.medicineDao = medicineDao;
	}
}
