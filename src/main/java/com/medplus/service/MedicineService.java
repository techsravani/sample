package com.medplus.service;

import java.util.List;

import com.medplus.dto.MedicinesDto;

public interface MedicineService {
	List<MedicinesDto> getMedicines();
}
