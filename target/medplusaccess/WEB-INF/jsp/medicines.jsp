<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>List of Medicines</title>
	</head>
	<body style="font-family: consolas; font-size: 20px;">
		<table>
			<tr>
				<th>medicine name</th>
				<th>manufacturer</th>
				<th>price</th>
				<th>quantity</th>
			</tr>
			<c:forEach items="${medicines}" var="medicine">
				<tr>
					<td>${medicine.medicineName}</td>
					<td>${medicine.manufacturer}</td>
					<td>${medicine.price}</td>
					<td>${medicine.quantity}</td>
				</tr>
			</c:forEach>
		</table>
	
	</body>
</html>