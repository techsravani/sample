<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>MedPlus Control Panel</title>
	</head>
	<body style="font-family: consolas; font-size: 20px;">
		<h2>Medplus Medicines Info</h2>
		<a href="${pageContext.request.contextPath}/medicines.access">Medicines</a>
	</body>
</html>